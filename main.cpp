#include <iostream>
#include <stack>
#include <vector>
#include <iterator>
#include <cmath>


using namespace std;


// check if elem is symbol
bool is_symbol(char sym)
{
    switch (sym)
    {
        case '+': return true;
        case '-': return true;
        case '(': return true;
        case '^': return true;
        case '/': return true;
        case '*': return true;
    }
    return false;
}

// operators with first priority
bool is_low_priority(char sym)
{
    switch (sym)
    {
        case '-': return true;
        case '+': return true;
    }
    return false;
}

// operators with second priority
bool is_mid_priority(char sym)
{
    switch (sym)
    {
        case '/': return true;
        case '*': return true;
    }
    return false;
}

// opertator with highest priority
bool is_high_priority(char sym)
{
    return sym == '^' ? true : false;
}

string InputData()
{
    cout << endl;
    cout << "Enter expression: ";
    char input[100];
    cin.getline(input, sizeof(input));
    // convert data from char to string
    string result = input;
    return result;
}

int main()
{
    // expression which must enter user
    string expression;
    // elemets for stack
    stack<char> symbols;
    vector<char> exit_string;
    stack<double> result;
    double buffer = 0;
    
    expression = InputData();
    
    // use special iterators for string
    for (string::iterator it = expression.begin(); it != expression.end(); ++it)
    {
        // set element to exit_string
        if (isdigit(*it)) exit_string.push_back(*it);
        // chech if elem is symbol
        else if (is_symbol(*it))
        {
            // if priority of symbol is 1
            if (is_low_priority(*it))
            {
                // if is symbol or symbol has higer priority then 1
                if (symbols.size() && (is_mid_priority(symbols.top()) || is_high_priority(symbols.top())))
                {
                    // push data into postfix
                    exit_string.push_back(symbols.top());
                    symbols.pop();
                }
                // set element to temp stack
                symbols.push(*it);
            }
            // chek even if entered symbol is second priority
            else if (is_mid_priority(*it))
            {
                // check if symbol has size and is't hider priority
                if (symbols.size() && is_high_priority(symbols.top()))
                {
                    // set to exit string
                    exit_string.push_back(symbols.top());
                    // delete symbol from stack
                    symbols.pop();
                }
                // push into stack symbol of string
                symbols.push(*it);
            }
            // if symbol has highes priority
            else if (is_high_priority(*it))
            {
                // set symbol to stack
                symbols.push(*it);
            }
            else symbols.push(*it);
        }
        // if symbolt is bracket
        else if (*it == ')')
        {
            // set all symbols from stack to exiting string
            while (symbols.top() != '(')
            {
                exit_string.push_back(symbols.top());
                symbols.pop();
            }
            if (symbols.size()) { symbols.pop(); }
        }
    }
    // if we set all symbols from stack to exiting string
    while (symbols.size())
    {
        exit_string.push_back(symbols.top());
        symbols.pop();
    }
    
    // make duplicate of exit_string
    copy(exit_string.begin(), exit_string.end(), std::ostream_iterator<char>(std::cout, " "));
    
    // reading exiting string by characters
    for (vector<char>::iterator it = exit_string.begin(); it != exit_string.end(); ++it)
    {
        // if symbol (character) is digit
        if (isdigit(*it))
        {
            // push digit to result string
            result.push(*it - '0');
        }
        else
            // making computing operations
            switch (*it)
        {
            case '+': { buffer = result.top(); result.pop(); result.top() += buffer; break; }
            case '-': { buffer = result.top(); result.pop(); result.top() -= buffer; break; }
            case '*': { buffer = result.top(); result.pop(); result.top() *= buffer; break; }
            case '/': { buffer = result.top(); result.pop(); result.top() /= buffer; break; }
            case '^': { buffer = result.top(); result.pop(); result.top() = pow(result.top(), buffer); break; }
        }
    }
    cout << "\nANSWER: " << result.top() << endl;
    
    system("pause");
    return 0;
}
